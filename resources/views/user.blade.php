<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker-standalone.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css">
    <script
        src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="https://momentjs.com/downloads/moment.min.js"></script>
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript" charset="utf8"

            src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
</head>
<body>
<div class="container">
    <h1 class="text-center mt-5">Yolearn User</h1>
    <form action="">
        <div class="row">
            <div class="form-group">
                <div class='col-sm-12'>
                    <input type='text' class="form-control datetimepicker2" name="date_start" id="dateStart" placeholder="date start"/>

                </div>

            </div>
            <div class="form-group">
                <div class='col-sm-12'>
                    <input type='text' class="form-control datetimepicker2" name="date_end" id="dateEnd" placeholder="date end"/>
                </div>
            </div>
            <div class="form-group">
                <button type="button" id="btnSearch" class="btn btn-success">Tìm kiếm</button>
                <button type="button" id="btnExport" class="btn btn-success">Export excel</button>
            </div>
        </div>
    </form>
    <div class="table-responsive">
        <table id="table" class="table">
            <thead>
            <tr>
                <th>STT</th>
                <th>name</th>
                <th>email</th>
                <th>mobile</th>
                <th>gender</th>
                <th>birthday</th>
                <th>created_at</th>
            </tr>
            </thead>
        </table>
    </div>
</div>
<script>
    $(function () {
        $('#btnExport').click(function () {
            let query = {
                date_start: $('#dateStart').val(),
                date_end: $('#dateEnd').val(),
            };
            window.location = "{{ route('export')  }}?" + $.param(query);
        });
        $('.datetimepicker2').datetimepicker({
            format:'DD/MM/YYYY HH:mm:ss',
        });
        $('#btnSearch').click(function () {

            $.ajax('/search', {
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                method: 'POST',
                methodType: 'JSON',
                data: {
                    date_start: $('#dateStart').val(),
                    date_end: $('#dateEnd').val(),

                },
                success: function (res) {
                    $('#table').DataTable().clear().destroy();
                    oTable = jQuery('#table').DataTable({
                        data: res,
                        "autoWidth": false,
                        destroy: true,
                        searching: false,
                        columns: [
                            {
                                data: 'id', name: 'id', class: 'text-center',
                                render: function (data, type, row, meta) {
                                    return meta.row + meta.settings._iDisplayStart + 1;
                                }
                            },
                            {
                                data: "name", sortable: false,
                                render: function (data, type, full, meta) {
                                    return data;
                                }
                            },
                            {
                                data: "email", sortable: false,
                                render: function (data, type, full, meta) {
                                    return data;
                                }
                            },
                            {
                                data: "mobile", sortable: false,
                                render: function (data, type, full, meta) {
                                    return data;
                                }
                            },
                            {
                                data: "gender", sortable: false,
                                render: function (data, type, full, meta) {
                                    return data;
                                }
                            },
                            {
                                data: "birthday", sortable: false,
                                render: function (data, type, full, meta) {
                                    return data;
                                }
                            },
                            {
                                data: "created_at", sortable: false,
                                render: function (data, type, full, meta) {
                                    return data;
                                }
                            },
                        ]
                    })
                },
                error: function (err) {
                    console.log(err);
                }
            });
        })
    });
</script>
</body>
</html>
