<?php

namespace App\Exports;


use App\Models\ClassBlockFrameSubject;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class UserExport implements FromCollection, WithHeadings
{
    private $dateStart, $dateEnd;

    public function __construct($dateStart, $dateEnd)
    {
        $this->dateEnd = $dateEnd;
        $this->dateStart = $dateStart;
    }

    public function collection()
    {
        $query = DB::table('users')
            ->where('created_at', '>=',$this->dateStart )->where('created_at','<=',$this->dateEnd)
            ->select('name', 'email', 'mobile', 'gender', 'birthday', 'created_at');
        $select = $query->get();

        $result = [];

        foreach ($select as $key => $item) {
            $tmp = [];
            $tmp[0] = $item->name;
            $tmp[1] = $item->email;
            $tmp[2] = $item->mobile;
            $tmp[3] = $item->gender;
            $tmp[4] = $item->birthday;
            $tmp[5] = $item->created_at;
            $result[] = $tmp;
        }
        return (collect($result));
    }

    public function headings(): array
    {
        return ['name', 'email', 'mobile', 'gender', 'birthday', 'created_at'];
    }
}
