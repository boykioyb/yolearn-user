<?php

namespace App\Http\Controllers;

use App\Exports\UserExport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class UserController extends Controller
{

    public function index(Request $request)
    {
        $date = date('Y-m-d H:i:s');

        return view('user',compact('date'));
    }

    public function search(Request $request)
    {
        $dateStart = str_replace('/','-',$request->get('date_start'));
        $dateEnd = str_replace('/','-',$request->get('date_end'));
        $dateStart = date('Y-m-d H:i:s', strtotime($dateStart));
        $dateEnd = date('Y-m-d H:i:s', strtotime($dateEnd));
        $query = DB::table('users')
            ->where('created_at', '>=',$dateStart)->where('created_at','<=',$dateEnd)
            ->select('name', 'email', 'mobile', 'gender', 'birthday', 'created_at');
        $result = $query->get();
        return response()->json($result);
    }
    public function test(){
        $query = DB::table('users')
            ->select('name', 'email', 'mobile', 'gender', 'birthday', 'created_at')->limit(5);
        $result = $query->get();
        dump($result);die;
        return response()->json($result);
    }
    public function export(Request $request){
        $dateStart = str_replace('/','-',$request->get('date_start'));
        $dateEnd = str_replace('/','-',$request->get('date_end'));
        $dateStart = date('Y-m-d H:i:s', strtotime($dateStart));
        $dateEnd = date('Y-m-d H:i:s', strtotime($dateEnd));
        return Excel::download(new UserExport($dateStart, $dateEnd), 'user_yolearn_' . date('Y_m_d') . '_' . time() . '.xlsx');
    }
}
